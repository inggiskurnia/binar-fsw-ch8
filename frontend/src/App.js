import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom";
import Dashboard from "./Dashboard";
import AddPlayer from "./AddPlayer";
import EditPlayer from "./EditPlayer";
import './App.css';

let coba;

function App() {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Dashboard/>}></Route>
        <Route path='/player/add' element={<AddPlayer/>}></Route>
        <Route path='/player/edit/:playerid' element={<EditPlayer/>}></Route>
      </Routes>
    </Router>
  );
}

export default App;
