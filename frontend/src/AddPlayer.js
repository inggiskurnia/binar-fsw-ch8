import {Component} from "react";
import {Link} from "react-router-dom";

class EditPlayer extends Component {

    addPlayer() {

        let inputAddedPlayer = {};
        inputAddedPlayer.username = document.getElementById('username').value;
        inputAddedPlayer.email = document.getElementById('email').value;
        inputAddedPlayer.password = document.getElementById('password').value;
        inputAddedPlayer.experience = document.getElementById('experience').value;
        inputAddedPlayer.lvl = document.getElementById('lvl').value;

        if (!inputAddedPlayer.username && !inputAddedPlayer.email && !inputAddedPlayer.password && !inputAddedPlayer.experience && !inputAddedPlayer.lvl) {
            alert("You must fill all of the data ! ");
            return;
        }

        fetch (`http://localhost:4000/api/v1/players/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputAddedPlayer),
        })
            .then(res => res.json())
            .then(json => console.log(json.result, json.message))
    }

    render() {
        return(
            <div className="container mt-3">
                <div className="d-flex justify-content-center">Add Player</div>
    
                <div className="row mt-3 ">
                    <div className="col form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" placeholder="username" />
                    </div>
                    <div className="col form-group">
                        <label htmlFor="email">Email address</label>
                        <input type="text" className="form-control" id="email" placeholder="email@site.com" />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col form-group">
                        <label htmlFor="password">Password</label>
                        <input type="text" className="form-control" id="password" placeholder="*****" />
                    </div>
    
                    <div className="col form-group">
                        <label htmlFor="experience">Experience</label>
                        <input type="text" className="form-control" id="experience" placeholder="1000" />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-6 form-group">
                        <label htmlFor="lvl">Level</label>
                        <input type="text" className="form-control" id="lvl" placeholder="1" />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-1">
                        <button type="button" onClick={()=>this.addPlayer()} className="btn btn-primary">Submit</button>
                    </div>
                    <Link to="/" className="ml-4 col-1">
                        <button type="button" className="btn btn-success" >Back</button>
                    </Link>
                </div>
            </div>
        )
    }
}

export default EditPlayer