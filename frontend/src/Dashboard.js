import {Component} from "react";
import {Link} from "react-router-dom";

class Dashboard extends Component {
    state = {
        playersData : ''
    }

    findPlayer() {

        let inputFilter = {};
        inputFilter.username = document.getElementById('username').value;
        inputFilter.email = document.getElementById('email').value;
        inputFilter.experience = document.getElementById('experience').value;
        inputFilter.lvl = document.getElementById('lvl').value;

        if (!inputFilter.username && !inputFilter.email && !inputFilter.experience && !inputFilter.lvl) {
            alert("You must fill at least one filter category ");
            return;
        }

        const filterKeys = Object.keys(inputFilter);
        const filterValues = Object.values(inputFilter);
        let filterData = '';

        for(let i=0; i < filterKeys.length; i++){
            if (filterValues[i]) filterData += `${filterKeys[i]}=${filterValues[i]}&&`
        }

        fetch (`http://localhost:4000/api/v1/players?${filterData}`)
            .then(res => res.json())
            .then(json => this.setState({playersData: json.data}))
    }

    deletePlayer(playerId) {
        fetch (`http://localhost:4000/api/v1/players/${playerId}`, {
            method: 'DELETE'
        })
            .then(res => alert("Player successfuly deleted"));
    }

    createElement(){
        let elements = [];
        for (let i=0; i < this.state.playersData.length; i++){
            let link = `/player/edit/${this.state.playersData[i].id}`
            elements.push(
                <tr>
                    <td>{this.state.playersData[i].id}</td>
                    <td>{this.state.playersData[i].username}</td>
                    <td>{this.state.playersData[i].email}</td>
                    <td>{this.state.playersData[i].experience}</td>
                    <td>
                        <Link to={link}>Edit</Link>
                        <a href="/" onClick={()=>this.deletePlayer(this.state.playersData[i].id)} className="ml-2 text-danger">Delete</a>
                    </td>
                </tr>
             )
        }
        return elements
    }

    render(){
        return(
            <div className="container mt-3">
                <div className="row mt-3 ">
                    <div className="col form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" placeholder="username" />
                    </div>
                    <div className="col form-group">
                        <label htmlFor="email">Email address</label>
                        <input type="text" className="form-control" id="email" placeholder="email@site.com" />
                    </div>
                    <div className="col form-group">
                        <label htmlFor="experience">Experience</label>
                        <input type="text" className="form-control" id="experience" placeholder="1000" />
                    </div>
                    <div className="col form-group">
                        <label htmlFor="lvl">Level</label>
                        <input type="text" className="form-control" id="lvl" placeholder="1" />
                    </div>
                    <div className="col form-group d-flex align-items-end">
                        <button type="button" onClick={()=>this.findPlayer()} className="btn btn-light form-control">Search</button>
                    </div>
                </div>
    
                <div className="row mt-4">
                    <div className="col d-flex justify-content-end">
                        <Link to="/player/add">
                            <button type="button" className="btn btn-success" >Add Player</button>
                        </Link>
                    </div>
                </div>
    
                <table className="table mt-3">
                <thead className="thead-dark">
                    <tr>
                    <th scope="col">id</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">Experience</th>
                    <th scope="col">Level</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.createElement()}
                </tbody>
                </table>
            </div>
        )
    }
}

export default Dashboard